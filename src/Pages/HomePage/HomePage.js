import React from "react";
import Banner from "./Banner/Banner";
import ListFilm from "./ListFilm/ListFilm";

export default function HomePage() {
  return (
    <>
      <div className="mt-24">
        <Banner />
      </div>
      <div className="pt-12 ">
        <div className="container mx-auto">
          <ListFilm />
        </div>
      </div>
    </>
  );
}
