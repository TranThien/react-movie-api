import React, { useEffect, useState } from "react";
import { movieService } from "../../../service/movieService";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function ListFilm() {
  const [listMovie, setListMovie] = useState([]);

  useEffect(() => {
    movieService.getListMovie().then((res) => {
      setListMovie(res.data.content);
    });
  }, []);
  const renderListFilm = () => {
    return listMovie.slice(0, 16).map((item) => {
      return (
        <Card
          key={item.maPhim}
          hoverable
          style={{
            width: "240",
          }}
          cover={
            <img
              className="h-64 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta
            title={<h2 className="text-center">{item.tenPhim}</h2>}
            description={
              <p className="text-center h-11">
                {item.moTa.length > 60
                  ? item.moTa.slice(0, 60) + "..."
                  : item.moTa}
              </p>
            }
          />
          <div className="text-center">
            <NavLink
              to={`/detail/${item.maPhim}`}
              className="text-slate-50 bg-red-500 py-3 px-5 mr-2 rounded inline-block"
            >
              Xem chi tiết
            </NavLink>
            <NavLink className="text-slate-50 bg-red-500 py-3 px-5 ml-2 rounded inline-block">
              Mua vé
            </NavLink>
          </div>
        </Card>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-3">{renderListFilm()}</div>;
}
