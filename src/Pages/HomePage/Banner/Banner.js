import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { movieService } from "../../../service/movieService";

export default function Banner() {
  const contentStyle = {
    height: "500px",
    width: "100%",
    color: "#fff",
    lineHeight: "500px",
    textAlign: "center",
  };
  const [banner, setBanner] = useState([]);
  useEffect(() => {
    movieService
      .getBanner()
      .then((res) => {
        setBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderBanner = () => {
    return banner.map((item, index) => {
      return (
        <div key={index}>
          <div style={contentStyle}>
            <img src={item.hinhAnh} className="w-full h-full block" alt="" />
          </div>
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderBanner()}</Carousel>;
}
