import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../service/movieService";
import styles from "./detail.module.css";

export default function DetailPage() {
  const detail = useParams();
  const [detailFilm, setDetailFilm] = useState(null);
  console.log(detailFilm);
  useEffect(() => {
    movieService.getDetailMovie(detail.maPhim).then((res) => {
      setDetailFilm(res.data.content);
    });
  }, [detail.maPhim]);

  return (
    <div className={`  ${styles.detailPage}`}>
      <div className=" flex items-center mt-10  container mx-auto">
        <div>
          <img
            src={detailFilm?.hinhAnh}
            alt=""
            className="w-full object-cover block h-96"
          />
        </div>
        <div>
          <h2>{detailFilm?.tenPhim}</h2>
          <p>{detailFilm?.ngayKhoiChieu}</p>
        </div>
        <div>sâsasasasasas</div>
      </div>
    </div>
  );
}
