import React from "react";
import { Button, Checkbox, Form, Input } from "antd";
import styles from "./register.module.css";
import Lottie from "lottie-react";
import bg_plane from "../../asset/61111-plane.json";
import { NavLink } from "react-router-dom";

export default function RegisterPage() {
  const onFinish = (values) => {
    console.log("Success:", values);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const renderForm = () => {
    return (
      <Form
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="matKhau"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Please input your email!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Phone-Number"
          name="soDt"
          rules={[
            {
              required: true,
              message: "Please input your phone number!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Full Name"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Please input your full name!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        {/* <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item> */}

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            className="bg-teal-300 text-red-400 border-none"
            htmlType="submit"
          >
            ĐĂNG KÝ
          </Button>
        </Form.Item>
      </Form>
    );
  };
  return (
    <div className={styles.bg}>
      <div className="absolute  bottom-1/2 right-1/2 h-2/3 items-center translate-y-2/4 translate-x-2/4 bg-white flex  flex-col justify-center">
        <h1 className="text-2xl text-red-500 text-center mb-10"> Đăng Ký</h1>
        <div className="flex justify-center items-center ">
          <div className="w-1/2">
            <Lottie animationData={bg_plane} loop={true} />
          </div>
          <div className="w-1/2 px-3">{renderForm()}</div>
        </div>
        <NavLink to="/login">
          <span className="text-fuchsia-800 hover:text-red-500">
            Bạn đã có tài khoản ?
          </span>
        </NavLink>
      </div>
    </div>
  );
}
