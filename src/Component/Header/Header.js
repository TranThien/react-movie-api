import React from "react";
import { NavLink } from "react-router-dom";
import styles from "./header.module.css";

export default function Header() {
  // const handleLogin = () => {
  //   window.location.href = "/login";
  // };
  // const handleRegister = () => {
  //   window.location.href = "/register";
  // };
  return (
    <>
      <header className="p-4 bg-gray-800 text-gray-100 fixed w-full z-30 top-0 left-0 right-0">
        <div className="container flex justify-between h-16 mx-auto">
          <NavLink
            rel="noopener noreferrer"
            to="/"
            aria-label="Back to homepage"
            className="flex items-center p-2"
          >
            <img
              src="https://www.cgv.vn/skin/frontend/cgv/default/images/cgvlogo-small.png"
              alt="CGV"
            />
          </NavLink>
          <ul className="items-stretch hidden space-x-3 lg:flex mb-0 ">
            <li className="flex">
              <NavLink
                rel="noopener noreferrer"
                to=""
                className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 active:text-violet-400 focus:border-violet-400"
              >
                Lịch Chiếu
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                rel="noopener noreferrer"
                to=""
                className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500  active:text-violet-400 focus:border-violet-400"
              >
                Cụm Rạp
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                rel="noopener noreferrer"
                to=""
                className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500  active:text-violet-400 focus:border-violet-400"
              >
                Tin Tức
              </NavLink>
            </li>
            <li className="flex">
              <NavLink
                rel="noopener noreferrer"
                to=""
                className="flex items-center px-4 -mb-1 border-b-2 border-transparent hover:text-red-500 active:text-violet-400 focus:border-violet-400 "
              >
                Ứng Dụng
              </NavLink>
            </li>
          </ul>
          <div className="items-center flex-shrink-0 hidden lg:flex">
            <NavLink
              // onClick={handleLogin}
              to="/login"
              className={`self-center px-8 py-3 rounded hover:text-red-500 inline-block ${styles.separate}`}
            >
              Đăng Nhập
            </NavLink>
            <NavLink
              to="/register"
              // onClick={handleRegister}
              className="self-center px-8 py-3 rounded hover:text-red-500 inline-block"
            >
              Đăng Ký
            </NavLink>
          </div>
          <button className="p-4 lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-6 h-6 dark:text-gray-100"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              ></path>
            </svg>
          </button>
        </div>
      </header>
    </>
  );
}
