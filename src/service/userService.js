// lấy thông tin người dùng đăng nhập

import { base_URL } from "./configURL";
//Cách viết 1 :  cách viết tạo các API chung 1 Object để dễ quản lí
export const userService = {
  postLogin: () => {
    return base_URL.post("/api/QuanLyNguoiDung/DangNhap");
  },
  postRegister: () => {
    return base_URL.post("/api/QuanLyNguoiDung/DangKy");
  },
};

// cách viết 2 : cách truyền thống
// export const postLogin = () => {
//     return base_URL.post("/api/QuanLyNguoiDung/DangNhap")
// }
