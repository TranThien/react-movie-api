import { base_URL } from "./configURL";

export const movieService = {
  getBanner: () => {
    return base_URL.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getListMovie: () => {
    return base_URL.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP05");
  },
  getDetailMovie: (data) => {
    return base_URL.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
  },
  getPagination: () => {
    return base_URL.get("/api/QuanLyPhim/LayDanhSachPhimPhanTrang");
  },
  getMovieDaily: () => {
    return base_URL.get("/api/QuanLyPhim/LayDanhSachPhimTheoNgay");
  },
};
