const USER_LOCAL = "USER_LOCAL";
// đặt 1 key dung chung để setItem
export const localStorage = {
  get: () => {
    //lấy dữ liệu lên
    let dataJSON = localStorage.getItem(USER_LOCAL);
    dataJSON ? JSON.parse(dataJSON) : null;
  },
  set: (userData) => {
    // convert dữ liệu từ Ob sang json
    let userJSON = JSON.stringify(userData);
    // lưu xuống local
    localStorage.setItem(USER_LOCAL, userJSON);
  },
  remove: () => {
    localStorage.removeItem(USER_LOCAL);
  },
};
