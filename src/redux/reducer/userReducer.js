import { USER_LOGIN } from "../constant/userContant";

const initialState = {
  user: null,
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      return { ...state, user: payload };

    default:
      return state;
  }
};
